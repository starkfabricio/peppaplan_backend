
import * as express from 'express'; // Importa o framework do express
import * as cors from 'cors';
import Package from './Package'; // Importa a Classe de Pacotes (*n Planos)

class App { // Classe responsável por iniciar a aplicação, como é uma rota só, coloquei o a rota no método

    public express: express.Application;

    constructor() {
        this.express = express(); // Instancia o express
        this.express.use(cors()); // Habilita o acesso ao cors nas rotas
        this.routes(); // Chama o método responsável pelas rotas
    }

    private routes(): void {

        let router = express.Router(); // Referencia as rotas do express

        router.get('/list-all-broadband', async (req, res) => { // Recebe e Resolve o Request da Rota
            try {
                const broadbands = await Package.getAll();
                return res.send(broadbands); // retorna a resposta da requisição com os pacotes
            } catch (error) {
                return res.status(400).send({ error: 'Erro ao carregar pacotes'});
            }
        });

        this.express.use('/', router); // referencia a rota a instancia do express

    }
}

export default new App().express; // exporta uma instancia do express