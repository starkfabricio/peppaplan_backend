import * as http from 'http'; // Importa o servidor HTTP
import App from './App'; // Importa uma instância do express

const port = 3000;
App.set('port', port); // seta a porta do express

const server = http.createServer(App); // Passa a instancia do express para o servidor http
server.listen(port); // Coloca o servidor http para escutar
