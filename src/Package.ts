import * as data from './Data/Plans.json'; // Importa o Json com os planos, se fosse dinâmico seria uma base de dados ou uma request

export class Package { // Classe responsável por tratar os pacotes com os planos

    plans; // guarda os planos
    bundles = []; // guarda os pacotes
    flux; // contador de fluxos de pacotes

    // Todo pacote inicia por uma "raiz" (tv, telefone, internet)

    getAll() {

        let content = Object.values(data); // pega os dados do Json

        // reseta os valores
        this.plans = content[0];
        this.bundles = [];
        this.flux = 0;

        // para todo plano, crie um pacote
        this.plans.forEach(plan => {
            this.createPackage(plan); // é feito uma chamada em uma método recursivo para criar os pacotes
        });

        this.bundles.sort((obj1, obj2) => { // ordena os pacotes por preço
            return obj1.price - obj2.price;
        });

        return this.bundles; // retorna os pacotes
    }

    createPackage(plan, history?) { // método recursivo responsável por criar pacotes a partir dos planos
                                    // a variavel history é usada para ir salvando a história do pacote a medida que ele avança
        if (plan.root && !history) { // se for um plano raiz
            this.flux++;
            this.bundles.push({ // adiciona o plano raiz na lista de pacotes,
                name: plan.name,
                price: plan.price,
                type: plan.type,
                root: true,
                flux: this.flux
            });

            if (plan['relations']) { // se o plano tiver algum relacionamento com adicionais ou outros planos
                let addons = []; // podem existir diversos adicionais
                plan['relations'].forEach(relation => { // Cria pacote sem adicionais
                    if (relation.type !== 'addon') { // a relação só entra no fluxo uma vez se não for adicional
                        this.createPackage(this.plans[relation.relation_id - 1], {
                            name: plan.name,
                            price: plan.price,
                            type: plan.type,
                            diference: relation.additionals + relation.discounts,
                            flux: this.flux
                        });
                    } else {
                        addons.push(relation); // os adicionais são salvos no array de adicionais
                    }
                });
                plan['relations'].forEach(relation => { // Cria pacote com adicionais, a formula é diferente
                    if (relation.type !== 'addon') {
                        addons.forEach(additional => {
                            this.createPackage(this.plans[relation.relation_id - 1], {
                                name: plan.name + ' + ' + additional.relation_name,
                                price: plan.price,
                                type: plan.type,
                                diference: relation.additionals + relation.discounts + additional.additionals + additional.discounts,
                                flux: this.flux
                            });
                        });
                    }
                });
            }
        }
        else
            if (history) { // se não for raiz e tiver uma história

                this.bundles.push({ // Adiciona um novo plano combinando com sua história (internet + tv...)
                    name: history.name + ' + ' + plan.name,
                    price: history.price + history.diference,
                    type: history.type,
                    flux: history.flux
                });

                if (plan['relations']) {
                    let addons = [];
                    plan['relations'].forEach(relation => { // Cria pacote sem adicionais, este fluxo não vai coloca nenhum adicional, somente os "raizes"
                        if (relation.type !== 'addon') {
                            this.createPackage(this.plans[relation.relation_id - 1], {
                                name: history.name + ' + ' + plan.name,
                                price: history.price + history.diference,
                                type: history.type,
                                diference: relation.additionals + relation.discounts,
                                flux: history.flux
                            });
                        } else {
                            addons.push(relation); // adiciona a lista de relações
                        };
                    });
                    plan['relations'].forEach(relation => { // Cria pacote com adicionais, este fluxo vai colocar os adicionais aos pacotes
                        if (relation.type !== 'addon') {
                            addons.forEach(additional => {
                                this.createPackage(this.plans[relation.relation_id - 1], {
                                    name: history.name + ' + ' + plan.name + ' + ' + additional.relation_name,
                                    price: history.price + history.diference, // verificar calculo
                                    type: history.type,
                                    diference: relation.additionals + relation.discounts + additional.additionals + additional.discounts,
                                    flux: history.flux
                                });
                            });
                        }
                    });
                }
            }
    }
}
export default new Package();