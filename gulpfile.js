var gulp = require("gulp");
var ts = require("gulp-typescript");
var tsProject = ts.createProject("tsconfig.json");
var JSON_FILES = ['src/*.json', 'src/**/*.json'];

gulp.task("default", ["json"], () => {
    return tsProject.src()
        .pipe(tsProject())
        .js.pipe(gulp.dest("build"));
});

gulp.task('json', function () {
    return gulp.src(JSON_FILES)
        .pipe(gulp.dest('build'));
});

gulp.task('watch', ["default"], () => {
    gulp.watch('src/**/*.ts', ['default']);
});